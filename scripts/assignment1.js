var isMetric = true;

function calculateBMR(weightKg, heightCm, age, gender) {
    var bmr = (10 * weightKg) + (6.25 * heightCm) - (5 * age);
    gender = gender.toLowerCase();

    if ("male" === gender) {
        bmr += 5;
    }
    else {
        bmr -= 161;
    }

    return bmr;
}

function displayResults() {
    var div = document.getElementById("results");
    div.innerHTML = "Your BMR is something.";
    div.style.display = "block";
}

function switchToMetric() {
    document.getElementById("lblHeight1").innerHTML = "Metres";
    document.getElementById("lblHeight2").innerHTML = "Centimetres";
    document.getElementById("lblWeight1").innerHTML = "Kilograms";
    document.getElementById("lblWeight2").innerHTML = "Grams";

    document.getElementById("height2").max = "99";
    document.getElementById("weight2").max = "99";

    isMetric = true;
}

function switchToImperial() {
    document.getElementById("lblHeight1").innerHTML = "Feet";
    document.getElementById("lblHeight2").innerHTML = "Inches";
    document.getElementById("lblWeight1").innerHTML = "Stones";
    document.getElementById("lblWeight2").innerHTML = "Pounds";

    document.getElementById("height2").max = "11";
    document.getElementById("weight2").max = "9999";

    isMetric = false;
}

function feetToMetres(feet) {
    return feet * 0.3048;
}


