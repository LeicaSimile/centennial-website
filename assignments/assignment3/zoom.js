/*    JavaScript 6th Edition
 *    Chapter 5
 *    Chapter case

 *    Photo zoom
 *    Variables and functions
 *    Author: 
 *    Date:   

 *    Filename: zoom.js
 */

"use strict"; // interpret document contents in JavaScript strict mode

/* global variables */
var photoOrderArray = window.opener.photoOrder;
var figFilename = "images/IMG_0" + photoOrderArray[2] + ".jpg";
var figSmallFilename = "images/IMG_0" + photoOrderArray[2] + "sm.jpg";
var favIndex = 1; // Can be 1 to 4

/* populate img element and create event listener */
function pageSetup() {
    document.getElementsByTagName("img")[0].src = figFilename; // assign filename to img element
    createEventListener("closeLink", closeWin); // Listen for close link
    createEventListener("addLink", onAddLink_click); // Listen for adding favourites
}

/* close window */
function closeWin() {
    window.close();
}

/* CHANGED - create event listener for buttons/links */
function createEventListener(elementId, handlerFunction) {
    var closeWindowDiv = document.getElementById(elementId);
    if (closeWindowDiv.addEventListener) {
        closeWindowDiv.addEventListener("click", handlerFunction, false);
    } else if (closeWindowDiv.attachEvent) {
        closeWindowDiv.attachEvent("onclick", handlerFunction);
    }
}

/* add img src value and create event listener when page finishes loading */
window.onload = pageSetup;

/* All student code is to be placed below this comment. */
function onAddLink_click() {
    if (favIndex < 4) {
        var sectionFavourites = window.opener.document.getElementById("fav" + favIndex);
        var newFav = window.opener.document.createElement("img");
        newFav.src = figSmallFilename;
        
        sectionFavourites.appendChild(newFav);

        favIndex++;
    }
}