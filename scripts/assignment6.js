/* Assignment 6 - AJAX and JQuery Image Gallery
   By Angelica Catalan
*/

"use strict";

var imageIndex = 0;
var images = [];

function cyclePrevious() {
    
}

function cycleNext() {
    if (imageIndex == 9) {
        imageIndex = 1;
    }
    else {
        imageIndex += 1;
    }
}

function displayData() {

}

function displayImage() {
    
}

function getImages() {
    $.ajax({
        url: "assign06.json",
        dataType: "json",
        success: function(result) {
            images = result;
            setImages();
        }
    });
}

function onRadioChange(event) {
    imageIndex = $("input[type=radio]").index(event.target);
}

function setImages() {
    $("#currentImage").attr("src", images[imageIndex]["url"]);
}

function updateGallery() {
    getImages();
}

function initialize() {
    $("#leftarrow").click(cyclePrevious);
    $("#rightarrow").click(cycleNext);
    $("input[type=radio]").change(onRadioChange);
    updateGallery();
}

$(document).ready(initialize);