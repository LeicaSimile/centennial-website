/* Assignment 5 - Bug Smasher
   By Angelica Catalan
*/

"use strict";

var canvas = document.getElementById("game");
var context = canvas.getContext("2d");
var scoreLabel = document.getElementById("score");
var timerLabel = document.getElementById("timer");

var bug = {
    x: 0,
    y: 0,
    width: 0,
    height: 0,
};

const startingInterval = 3;
const startingBugSpeed = 200;
const intervalMultiplier = 0.9;
var hopInterval = 0;  // Seconds
var countdown = hopInterval;  // Current amount of seconds until bug moves
var score = 0;
var timeStart = 0;  // Time interval was reset

// Initialize canvas images
var backgroundReady = false;
var backgroundImage = new Image();
addEvent(backgroundImage, "load", function() { backgroundReady = true; });
backgroundImage.src = "images/assignment5-background.png";

var bugReady = false;
var bugImage = new Image();
addEvent(bugImage, "load", function() { bugReady = true; });
bugImage.src = "images/ladybug-60x60.png";

// Wrapper for listening to events
function addEvent(element, event, handler) {
    if (element.addEventListener) {
        element.addEventListener(event, handler, false);
    } else if (element.attachEvent) {
        element.attachEvent("on" + event, handler);
    }
}

function isBugClicked(event) {
    // Get coordinates of mouse relative to canvas
    var mouseX = event.pageX - canvas.offsetLeft;
    var mouseY = event.pageY - canvas.offsetTop;

    // See if mouse coords are within the area of the bug
    var isWithinX = false;
    var isWithinY = false;

    if (bug.x <= mouseX && mouseX <= bug.x + bug.width) {
        isWithinX = true;
    }
    if (bug.y <= mouseY && mouseY <= bug.y + bug.height) {
        isWithinY = true;
    }

    if (isWithinX && isWithinY) {
        // TODO: Check if coord is on a transparent portion of image
        return true;
    }
    else {
        return false;
    }
}

// Main loop
function main() {
    updateTimer();

    if (countdown <= 0) {
        moveBug();
        timeStart = Date.now();
        countdown = hopInterval;
    }
}

// Move bug to random location within canvas
function moveBug() {
    var border = 30;  // Bug must be at least this many pixels away from canvas border

    bug.x = border  + (Math.random() * (canvas.width - border * 2));
    bug.y = border  + (Math.random() * (canvas.height - border * 2));

    renderAll();
}

function onBugClick() {
    hopInterval *= intervalMultiplier;
    moveBug();

    score += 1;
    timeStart = Date.now();
    updateScore();
    updateTimer();
}

function onMouseClick(event) {
    if (isBugClicked(event)) {
        onBugClick();
    }
}

function renderAll() {
    renderImage(backgroundReady, backgroundImage, 0, 0);
    renderImage(bugReady, bugImage, bug.x, bug.y);
}

function renderImage(readyStatus, image, x, y) {
    if (readyStatus) {
        context.drawImage(image, x, y);
    }
}

function resetSpeed() {
    hopInterval = startingInterval;
}

// Set everything back to initial states
function restart() {
    resetSpeed();
    score = 0;

    moveBug();
    updateScore();

    timeStart = Date.now();
    setInterval(main, 100);
}

function updateScore() {
    scoreLabel.textContent = "Bugs smashed: " + score;
}

function updateTimer() {
    countdown = (Date.now() - timeStart) / 1000;
    countdown = hopInterval - countdown;
    if (countdown < 0) {
        countdown = 0;
    }
    timerLabel.textContent = countdown.toFixed(2);
}

function initialize() {
    bug.width = bugImage.width;
    bug.height = bugImage.height;

    restart();
    moveBug();
    renderAll();

    var newGameButton = document.getElementById("newGame");
    var resetSpeedButton = document.getElementById("resetSpeed");
    
    addEvent(newGameButton, "click", restart);
    addEvent(resetSpeedButton, "click", resetSpeed);
    addEvent(canvas, "click", onMouseClick);
}

addEvent(window, "load", initialize);