document.getElementById("makeTable").addEventListener("click", writeTable);

function clearElement(elementId) {
    document.getElementById(elementId).innerHTML = "";
}

function writeTable() {
    clearElement("userTable");

    var divTable = document.getElementById("userTable");
    var table = document.createElement("table");
    var tableBody = document.createElement("tbody");

    table.appendChild(tableBody);

    var numRows = parseInt(document.getElementById("numRows").value, 10);
    var numColumns = parseInt(document.getElementById("numCols").value, 10);

    for (var i = 1; i <= numRows; i++) {
        var row = tableBody.insertRow();

        for (var j = 1; j <= numColumns; j++) {
            var cell = row.insertCell();
            cell.innerHTML = i + "," + j;
        }
    }
    divTable.appendChild(table);
}